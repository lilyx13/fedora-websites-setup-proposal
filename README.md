# Fedora Websites Stack Proposal

This is a minimal setup of a potential stack for fedora's web and apps.

## Frontend Notes
- There are a few ways to go about SSG and SSR. Vue supports SSR as seen in the [Vue SSR Doc](https://vuejs.org/guide/scaling-up/ssr.html)
  - however SSG would be more involved with this setup. We would likely need to use something like [Vite-SSG](https://github.com/antfu/vite-ssg) which would also mean using [vite](https://vitejs.dev/) instead of [webpack](https://webpack.js.org/)
- Personally I think that we should stick with Webpack because of it's stability, good performance, big ecosystem. Vite is really fast, especially with dev servers, but it's a lot newer and not necessarily better for what we will need. **I'm curious about other peoples' input on this**
  - [A doc on configuring webpack in vue](https://cli.vuejs.org/guide/webpack.html#replacing-loaders-of-a-rule)
  - [Prerender](https://prerender.io/how-to-install-prerender/) **might be a good thing to look into for SSG**
- [This FARM stack (FastAPI React MongoDB)](https://www.mongodb.com/developer/languages/python/farm-stack-fastapi-react-mongodb/) tutorial offers some idea how we can make a **FAVS** Stack (FastAPI, Vue, Strapi/SQL).
- Vue might be a better choice for us than Nuxt for how much bigger the ecosystem is, it's more stable than Nuxt 3 (which is still in beta for at least the next month), and we are relying on less people for features and modules (I've encountered some vue plugins not working great with nuxt in the past)
  - However an argument in favour of Nuxt is that it has a very simple configuration and is really optimized

## Backend Notes
- A question that I have is how are we going to store data with the backend and to transfer to the front?
  - Similarly, what would be the best way to integrate this backend with a headless CMS for quick content edits. [Strapi](https://strapi.io/v4) is a good choice because it's self hosted and open source with a pretty big community