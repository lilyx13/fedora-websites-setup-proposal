import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import i18n from './plugins/i18n'
import head from './plugins/vue-head'

i18n(createApp(App)).use(router).use(head).mount('#app')
