from fastapi import FastAPI

app = FastAPI()

# Send Data to frontend routes
@app.get("/")
async def root():
  return {"msg": "simple test"}